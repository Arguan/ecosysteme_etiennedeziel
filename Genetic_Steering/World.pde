class World {
  final float FOOD_DENSITY = 1.2; // Food per 1000pix^2
  final float POISON_DENSITY = 0.35; 
  
  ArrayList<Vehicle> population = new ArrayList<Vehicle>();
  ArrayList<PVector> food = new ArrayList<PVector>();
  ArrayList<PVector> poison = new ArrayList<PVector>();
  
  int totPoi = 1; 
  int totFood = 1;
  
  World(int start) { 
    // start = starting population
    for(int i = 0; i < start; i++) {
    // Generate starting population (BLANK DNA)
      Vehicle v = new Vehicle(random(width), random(height), new DNA());
      population.add(v);
    }
    
    totFood = (int) FOOD_DENSITY * ((width * height) / 1000);
    totPoi = (int) POISON_DENSITY * ((width * height) / 1000);
    
    for(int i = 0; i < totFood; i++) {
      food.add(randomVector());
    }
    for(int i = 0; i < totPoi; i++) {
      poison.add(randomVector());
    }
  }
  
  void update() {
    // Generate new food/poison
    if(random(1) < 0.5) {
      food.add(randomVector());
    }
    if(random(population.size()) < population.size() * POISON_DENSITY * 0.2 && poison.size() < 100) {
      poison.add(randomVector());
    }
    
    // Display food / poison
    fill(0, 255, 0);
    for(PVector f : food) { ellipse(f.x, f.y, 5, 5); }
    fill(150, 0, 150);
    for(PVector p : poison) { ellipse(p.x, p.y, 5, 5); }
    
    // Iterate through vehicle updates
      for(int i = population.size()-1; i >= 0; i--) {
        Vehicle v = population.get(i);
        v.boundaries();
    
        v.addBehavior(food, poison,population);
        //population = v.killFlee(population);
        v.update();
        v.display(settings[0]);
    
        Vehicle newVehicle = v.clone();
        if(newVehicle != null) {
          population.add(newVehicle);
        }
    
        if(!v.isAlive()) {
          population.remove(i);
          food.add(new PVector(v.pos.x, v.pos.y)); // random food about death location
        } else {
          population.set(i, v);
        }
    }
  }
  
  PVector randomVector(){ return new PVector(random(width), random(height)); } 
}
