import java.math.*;
import java.util.*;

World world;

float timer;
float currentTime;


float maxfitness = 2;

boolean[] settings;

long f = 1;

void setup() {
  fullScreen(P2D);
  currentTime = millis();
  timer = 5000; //5 sec
  // Initiate settings
  // 0 == DEBUG
  // 1 == STATS
  settings = new boolean[2];
  for(int i = 0; i < settings.length; i++) {
    settings[i] = false;
  }
  
  world = new World(25);
}

void ToggleDebug(){
  if (millis() > currentTime + timer){
    timer += 5000;
    ToggleKeyDebug();
  }
}

void draw() {
  background(100);
  noStroke();
  ToggleDebug();
  world.update();
  
  f++;
}
void ToggleKeyDebug() {
   settings[0] = !settings[0]; // Toggle debug
}
void keyPressed() {
  if(keyCode == 68) { settings[0] = !settings[0]; } // Toggle debug (D)
}
 
 // Replacement for PVector.dist() function to improve optimisation
float distSq(PVector v1, PVector v2) {
  float fdist = sq(v2.x - v1.x) + sq(v2.y - v1.y);
  return fdist;
}
